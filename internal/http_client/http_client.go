package http_client

import (
	"io"
	"net/http"
)

type HttpRequestInterface interface {
	CreateRequest(method, url string, body io.Reader) (*http.Request, error)
	Do(req *http.Request) (*http.Response, error)
	Get(url string) (*http.Response, error)
}

type HttpRequestWrapper struct{}

func (hr *HttpRequestWrapper) Do(req *http.Request) (*http.Response, error) {
	client := &http.Client{}
	return client.Do(req)
}

func (hr *HttpRequestWrapper) CreateRequest(method, url string, body io.Reader) (*http.Request, error) {
	return http.NewRequest(method, url, body)
}

func (hr *HttpRequestWrapper) Get(url string) (*http.Response, error) {
	client := &http.Client{}
	return client.Get(url)
}
