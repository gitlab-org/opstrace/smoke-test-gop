package commons

import (
	"os"

	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/config"
)

const (
	GateKeeperHealthEndpoint   = "gatekeeper_endpoint"
	GouiHealthEndpoint         = "goui_endpoint"
	GrafanaApiToken            = "token"
	SentryDSN                  = "sentry_dsn"
	groupErrorTrackingEndpoint = "group_endpoint"
	errorTrackingIngDelaySec   = "0"
)

func InitializeConfig() {
	os.Setenv("GATEKEEPER_HEALTH_CHECK", GateKeeperHealthEndpoint)
	os.Setenv("GOUI_HEALTH_CHECK", GouiHealthEndpoint)
	os.Setenv("GRAFANA_API_TOKEN", GrafanaApiToken)
	os.Setenv("SENTRY_DSN", SentryDSN)
	os.Setenv("GROUP_ERROR_TRACKING_ENDPOINT", groupErrorTrackingEndpoint)
	os.Setenv("ERROR_TRACKING_INGESTION_DELAY_SEC", errorTrackingIngDelaySec)
	_ = config.InitializeConfig()
}
