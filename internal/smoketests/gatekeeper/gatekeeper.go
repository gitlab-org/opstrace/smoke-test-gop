package gatekeeper

import (
	"fmt"
	"net/http"

	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/config"
	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/http_client"
)

type GatekeeperTestRunner struct {
	Client http_client.HttpRequestInterface
}

func (g *GatekeeperTestRunner) RunUptimeTest() error {
	url := config.GetGateKeeperHealthEndpoint()
	resp, err := g.Client.Get(url)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("gatekeeper: url: %v, status code: %v, message: %v", url, resp.StatusCode, resp.Status)
	}

	return nil
}
