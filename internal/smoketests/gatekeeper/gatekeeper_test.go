package gatekeeper

import (
	"errors"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/http_client"
	commons "gitlab.com/gitlab-org/opstrace/smoke-test/internal/smoketests"
)

func TestGatekeeperRunUptimeTest(t *testing.T) {
	assert := assert.New(t)
	httpMock := &http_client.MockHttpClient{}
	gk := GatekeeperTestRunner{
		Client: httpMock,
	}
	commons.InitializeConfig()

	t.Run("Fails if Get returns an error", func(t *testing.T) {
		expectedErr := errors.New("mock-error")
		httpMock.On("Get", commons.GateKeeperHealthEndpoint).Times(1).Return(&http.Response{}, expectedErr)
		assert.Equal(expectedErr, gk.RunUptimeTest())
		httpMock.AssertExpectations(t)
	})

	t.Run("Fails if Get returns a non 200 status code", func(t *testing.T) {
		httpMock.On("Get", commons.GateKeeperHealthEndpoint).Times(1).Return(&http.Response{StatusCode: 400}, nil)
		assert.True(strings.Contains(gk.RunUptimeTest().Error(), "status code: 400"))
		httpMock.AssertExpectations(t)
	})

	t.Run("Succeeds", func(t *testing.T) {
		httpMock.On("Get", commons.GateKeeperHealthEndpoint).Times(1).Return(&http.Response{StatusCode: 200}, nil)
		assert.Nil(gk.RunUptimeTest())
		httpMock.AssertExpectations(t)
	})

}
