package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/config"
	"gitlab.com/gitlab-org/opstrace/smoke-test/internal/executor"
)

func init() {

	err := config.InitializeConfig()
	if err != nil {
		log.Fatal(err)
	}

}

func main() {

	executor := executor.PlatformExecutor{}
	err := executor.RunSmokeTest()
	if err != nil {
		fmt.Printf("RunSmokeTest failed: %v\n", err)
		err := notify(err)
		if err != nil {
			log.Printf("Notify failed with error: %v", err)
		}
		os.Exit(1)
	}
	log.Printf("Successfully executed smoke tests.")
	os.Exit(0)
}

func notify(err error) error {
	log.Println("Notified: ", err)
	return nil
}
